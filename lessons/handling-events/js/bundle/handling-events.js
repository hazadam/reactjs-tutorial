import React from 'react';
import ReactDOM from 'react-dom';

function chainComponent(component) {
    component.prototype = Object.create(React.Component.prototype);
    component.prototype.Component = component;
}

function Login() {
    React.Component.call(this, {});
    this.state = {
        username: '',
        password: '',
        loggedIn: false
    };
}

chainComponent(Login);

Login.prototype.render = function() {

    let loggedInMessage = null;

    if (this.state.loggedIn) {
        loggedInMessage = <h3>You're in!</h3>;
    }

    return (
        <form method="POST">
            <div>
                <input type="text" name="username" value={this.state.username} onChange={(e) => this.onInputChange(e)}></input>
                <input type="password" name="password" value={this.state.password} onChange={(e) => this.onInputChange(e)}></input>
            </div>
            <div>
                <button type="submit" onClick={(e) => this.onLoginClick(e)}>Submit</button>
            </div>
            {loggedInMessage}
        </form>
    );
}

Login.prototype.onInputChange = function(e) {
    let state = {};
    state[e.target.name] = e.target.value;
    this.setState(state);
}

Login.prototype.onLoginClick = function(e) {
    if (this.state.password === 'heslo' && this.state.username === 'uzivatel') {
        this.setState({
            loggedIn: true
        });
    }
}

ReactDOM.render(<Login/>, document.getElementById('root'));


import OnOff from './../components/OnOff';

ReactDOM.render(<OnOff/>, document.getElementById('onoff'));
