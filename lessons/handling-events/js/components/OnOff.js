import React from 'react';

function OnOff(props) {

    React.Component.call(this, props);
    this.state = {
        value: false
    };
}

OnOff.prototype = Object.create(React.Component.prototype);
OnOff.prototype.constructor = OnOff;

OnOff.prototype.render = function() {
    return (
        <button onClick={() => this.setState({ value: ! this.state.value })}>
            {this.state.value ? 'Off' : 'On'}
        </button>
    )
};

export default OnOff;