import React from 'react';
import ReactDOM from 'react-dom';

function FunctionComponent(props) {
    return (
        <div>
            <h2>This is rendered by a Function Component ({props.someProp})</h2>
        </div>
    );
}


ReactDOM.render(<FunctionComponent someProp="cool"></FunctionComponent>, document.getElementById('function-component'));


class ClassComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h2>This is renderd by a Class Component ({this.props.someProp})</h2>
            </div>
        );
    }
}

ReactDOM.render(<ClassComponent someProp="cool not so much"></ClassComponent>, document.getElementById('class-component'));

function SomeComponent(props) {
    return <h3>SomeComponent ({props.text})</h3>;
}

function AnotherComponent(props) {
    return <h3>AnotherComponent ({props.text})</h3>;
}

function ComposedComponent(props) {
    return (
        <div>
            <h1>{props.text}</h1>
            <SomeComponent text="Composition is"/>
            <AnotherComponent text="Very nice"/>
        </div>
    );
}

ReactDOM.render(<ComposedComponent text="Composing Components"/>, document.getElementById('composing-components'));