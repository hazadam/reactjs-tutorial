import React from 'react';
import TemperatureInput from './TemperatureInput';

function toCelsius(fahrenheit) {
    return (fahrenheit - 32) * 5 / 9;
}

function toFahrenheit(celsius) {
    return (celsius * 9 / 5) + 32;
}

function tryConvert(temperature, convert) {

    const input = parseFloat(temperature);

    if (Number.isNaN(input)) {
        return '';
    }

    const output = convert(input);
    const rounded = Math.round(output * 1000) / 1000;

    return rounded.toString();
}

function Calcualtor(props) {
    React.Component.call(this, props);
    this.state = {
        temperature: '',
        scale: 'c'
    };
}

Calcualtor.prototype = Object.create(React.Component.prototype);
Calcualtor.prototype.constructor = Calcualtor;

Calcualtor.prototype.render = function() {

    const scale = this.state.scale;
    const temperature = this.state.temperature;
    const celsius = scale === 'f' ? tryConvert(temperature, toCelsius) : temperature;
    const fahrenheit = scale === 'c' ? tryConvert(temperature, toFahrenheit): temperature;

    return (
        <div>
            <TemperatureInput 
                scale='c'
                temperature={celsius}
                onTemperatureChange={(temperature) => this.setState({scale: 'c', temperature})}
            />
            <TemperatureInput scale='f'
                scale='f'
                temperature={fahrenheit}
                onTemperatureChange={(temperature) => this.setState({scale: 'f', temperature})}
            />
        </div>
    );
};

export default Calcualtor;