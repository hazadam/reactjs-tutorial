import React from 'react';

const scaleNames = {
    c: 'Celsius',
    f: 'Fahrenheit'
  };

function TemperatureInput(props) {
    React.Component.call(this, props);
}

TemperatureInput.prototype = Object.create(React.Component.prototype);
TemperatureInput.prototype.constructor = TemperatureInput;

TemperatureInput.prototype.render = function() {
    return (
        <fieldset>
            <legend>Enter temperature in {scaleNames[this.props.scale]}</legend>
            <input 
                value={this.props.temperature}
                onChange={(e) => this.props.onTemperatureChange(e.target.value)}
            />
        </fieldset>
    );
};

export default TemperatureInput;