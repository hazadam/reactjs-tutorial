import React from 'react';

export default function(props) {
    const message = props.celsius >= 100 ? 'The water would  boil' : 'The water would not boil yet';
    return <p>{message}</p>;
}