import React from 'react';
import ReactDOM from 'react-dom';

const element = <h1>Hello world!</h1>;

ReactDOM.render(element, document.getElementById('just-element'));

const name = 'John Sanmez';
const element1 = <h1>Hello there, {name}</h1>;

ReactDOM.render(element1, document.getElementById('embedding-expressions'));

function formatName(user) {
    return user.firstName + ' Von ' + user.lastName;
}

const user = {
    firstName: 'Lorenzo',
    lastName: 'Mutterhorn',
    avatarUrl: 'https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png'
};

const element2 = (
    <h1>
        Hello there dear user, {formatName(user)}
    </h1>
);

ReactDOM.render(element2, document.getElementById('any-expression'));

const tabIndexElement = <div tabIndex="0">tabIndex="0"</div>;
const image = <img src={user.avatarUrl}></img>;

ReactDOM.render(<div>{tabIndexElement}{image}</div>, document.getElementById('attributes'));

const someText = "Whadup";

const elementWithChildren = (
    <div>
        <h3>I am a child</h3><br/>
        <p>{someText}</p>
    </div>
);

ReactDOM.render(elementWithChildren, document.getElementById('children'));

const XSS = prompt('Gimme something malicous');

const vulnerableElement = (
    <div>
        <p>Some nasty XSS could happen, but not with JSX</p>
        <div>{XSS}</div>
    </div>
);

ReactDOM.render(vulnerableElement, document.getElementById('xss'));

const prelement = <pre>{typeof vulnerableElement}{JSON.stringify(vulnerableElement)}</pre>;

ReactDOM.render(prelement, document.getElementById('inspect-element'));