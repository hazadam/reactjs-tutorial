import React from 'react';
import ReactDOM from 'react-dom';

const element = <h1>Hello, world</h1>;
ReactDOM.render(element, document.getElementById('root'));

function renderTime() {
    const timeElement = (
        <div>
            <h3>The current time is</h3>
            <p>{new Date().toLocaleTimeString()}</p>
        </div>
    );
    ReactDOM.render(timeElement, document.getElementById('updating'));
};

renderTime();

setInterval(renderTime, 1000);
