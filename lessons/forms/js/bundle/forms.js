import React from 'react';
import ReactDOM from 'react-dom';

function Form(props) {

    React.Component.call(this, props);

    this.state = {
        formStatus: '',
        checkbox: false,
        checked: false,
        name: '',
        text: '',
        select: '',
        file: null,
    };
}

Form.prototype = Object.create(React.Component.prototype);
Form.prototype.constructor = Form;

Form.prototype.onInputChange = function(e) {
    console.log('Changing input');

    let target = e.target;
    let state = {}; 
    state[target.name] = target.type === 'checkbox' ? target.checked : target.value;

    this.setState(state);
}

Form.prototype.onSubmit = function(e) {
    console.log('Submitting form');
}

Form.prototype.render = function() {

    const _ = () => (e) => this.onInputChange(e);

    return (
        <form onSubmit={(e) => this.onSubmit(e)}>
            <div>
                <h4>Form status</h4>
                {this.state.formStatus && 
                    <p>{this.state.formStatus}</p>
                }
            </div>
            <div>
                <input type="checkbox" name="checkbox" checked={this.state.checkbox} onChange={_()}></input>
            </div>
            <div>
                <input type="text" name="name" value={this.state.name} onChange={_()} placeholder="Naming"></input>
            </div>
            <div>
                <textarea value={this.state.text} name="text" onChange={_()} placeholder="Texting"></textarea>
            </div>
            <div>
                <select value={this.state.select} name="select" onChange={_()}>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                </select>
            </div>
            <div>
                <input type="file" name="file" onChange={_()} placeholder="File upload"></input>
            </div>
        </form>
    );
}

ReactDOM.render(<Form/>, document.getElementById('root'));

let nullInput = document.getElementById('null-input');

ReactDOM.render(<input value="hi" />, nullInput);

setTimeout(function() {
  ReactDOM.render(<input value={null} />, nullInput);
}, 2000);