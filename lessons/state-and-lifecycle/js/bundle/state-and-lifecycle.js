import React from 'react';
import ReactDOM from 'react-dom';

function Clock(props) {
    React.Component.call(this, props);
    this.state = {
        date: props.time
    };
}

Clock.prototype = Object.create(React.Component.prototype);
Clock.prototype.constructor = Clock;

Clock.prototype.render = function() {
    return (
        <div>
            <h4>What the time?</h4>
            <p>{this.state.date.toLocaleTimeString()}</p>
        </div>
    );
}

Clock.prototype.tick = function() {
    this.setState({
        date: new Date()
    });
};

Clock.prototype.componentDidMount = function() {

    this.timer = setInterval(() => this.tick(), 1000);
}

Clock.prototype.componentWillUnmount = function() {

    this.timer && clearInterval(this.timer);
}

ReactDOM.render(<Clock time={new Date}/>, document.getElementById('stateful-clock'));


function Child(props) {
    React.Component.call(this, props);
}

Child.prototype = Object.create(React.Component.prototype);
Child.prototype.constructor = Child;

Child.prototype.render = function() {
    return <div>{this.props.user.name}</div>;
}

function Parent(props) {
    React.Component.call(this, props);
    this.state = {
        list: props.list
    };
}

Parent.prototype = Object.create(React.Component.prototype);
Parent.prototype.constructor = Parent;

Parent.prototype.render = function() {

    let users = this.state.list.map(user => {
        return <Child key={user.name} user={user}/>;
    });

    return (
        <div>
            {users}
        </div>
    );
}

Parent.prototype.onClick = function(e) {
    let [pos, newName] = prompt('Gimme new name').split('.');
    let users = this.state.list;
    users[pos-1].name = newName;
    this.setState({
        list: users
    });
}

Parent.prototype.componentDidMount = function() {
    document.getElementById('change-name-button').onclick = (e) => this.onClick(e);
}

let users = [
    { name: 'adam' }, { name: 'whatever'}
];


ReactDOM.render(<Parent list={users}/>, document.getElementById('data-flow'));