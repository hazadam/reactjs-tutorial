import React from 'react';
import LogoutButton from './LogoutButton';
import LoginButton from './LoginButton';
import Greeting from './Greeting';

function LoginControll(props) {
    React.Component.call(this, props);
    this.state = {
        isLoggedIn: props.isLoggedIn ? props.isLoggedIn : false
    };
}

LoginControll.prototype = Object.create(React.Component.prototype);
LoginControll.prototype.constructor = LoginControll;

LoginControll.prototype.render = function() {

    let button = null;

    if (this.state.isLoggedIn) {
        button = <LogoutButton label={"Log out"} handleClickEvent={(e) => {this.setState({isLoggedIn: false})}}/>;
    } else {
        button = <LoginButton label={"Log in"} handleClickEvent={(e) => {this.setState({isLoggedIn: true})}}/>;
    }

    return (
        <div>
            <Greeting isLoggedIn={this.state.isLoggedIn}/>
            {button}
        </div>
    );
}

export default LoginControll;