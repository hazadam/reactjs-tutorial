import React from 'react';

export default function LogoutButton(props) {
    return (
        <button onClick={props.handleClickEvent}>
            {props.label}
        </button>
    );
}