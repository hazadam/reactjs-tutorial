import React from 'react';
import ReactDOM from 'react-dom';
import Greeting from './../components/Greeting';
import LoginControll from '../components/LoginControll';

ReactDOM.render(<Greeting isLoggedIn={false}/>, document.getElementById('greeting'));
ReactDOM.render(<LoginControll isLoggedIn={false}/>, document.getElementById('variables'));