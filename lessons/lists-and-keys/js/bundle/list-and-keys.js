import React from 'react';
import ReactDOM from 'react-dom';

const NumbersList = function(props) {
    return (
        <ul>
            {props.numbers.map((number) => {
                return <li>{number}</li>;
            })}
        </ul>
    );
}

ReactDOM.render(<NumbersList numbers={[1, 2, 3, 4]}/>, document.getElementById('list-of-numbers'));

const NumbersListWithKeys = function(props) {
    return (
        <ul>
            {props.numbers.map((number) => {
                return <li key={number.toString()}>{number}</li>;
            })}
        </ul>
    );
}

ReactDOM.render(<NumbersListWithKeys numbers={[5, 6, 7, 8]}/>, document.getElementById('list-of-numbers-with-keys'));

const ListItem = function(props) {
    return <li>{props.number}</li>;
}

const List = function(props) {
    return (
        <ul>
            {props.numbers.map((number) => {
                <ListItem number={number}/>
            })}
        </ul>
    );
}

ReactDOM.render(<NumbersListWithKeys numbers={[9, 10, 11, 12]}/>, document.getElementById('list-components'));