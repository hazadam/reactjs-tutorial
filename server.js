const express = require('express');
const app = express();
const path = require('path');
const fs = require('fs');
const { URL } = require('url');

const lessonsDirectory = path.join(__dirname, 'lessons');

app.use(express.static('public'));

app.get('/*', (req, res) => {

    const uri = req.protocol + '://' + req.get('Host') + req.originalUrl;
    const lesson = (new URL(uri)).pathname.split('/')[1];
    const lessonsPath = path.join(lessonsDirectory, lesson, 'index.html');

    if (fs.existsSync(lessonsPath)) {
        res.sendFile(lessonsPath);
    } else {
        res.send('No such lesson found! (${lesson})');
    }
});

app.listen(3000);