const entry = require('webpack-glob-entry');
 
module.exports = {
  mode: "development",
  entry: entry('./lessons/*/js/bundle/*.js'),
  output: {
    path: '/var/www/reactjs/public/js',
    publicPath: 'js',
    filename: '[name].bundle.js'
  },
  devtool: "source-map",
  module: {
    rules: [
      { 
        test: /\.(js|jsx)$/, 
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-react']
          }
        }

      }
    ]
  }
}